这是一个我（luwen）写的博客网站，我希望在我每次学习新技术的时候将我的新技术融入这个项目，并在这个项目中写下心得
                                                            
                                                            --luwen@19.9.25:0:00
1.项目结构
---
me_parent 项目
<br>
1. me_children_interface -> me_children_service 前端请求服务
2. me_manage_web 项目后台管理
3. me_manage_interface -> me_manage_service 后台服务接口
4. me_common 项目工具模块
5. me_dao 数据库连接模块   
6. me_demo_test 一些代码的测试实验

2.项目技术
---
1. spring 
2. springmvc 
3. dubbo 
4. zookeeper
5. kafka
6. mybaits
                                                      