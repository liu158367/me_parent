package com.me.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.me.service.BlogService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class IndexCtroller {
    @GetMapping("/")
    public @ResponseBody
    String index(){
        return "ok";
    }
}
