package com.me.service.imp;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class Kafkaservice {

        static final Logger logger = LoggerFactory.getLogger(Kafkaservice.class);

        public void processMessage(Map<String, Map<Integer, String>> msgs) {
            for (Map.Entry<String, Map<Integer, String>> entry : msgs.entrySet()) {
                System.out.println("Consumer Message received: ");
                logger.info("Suchit Topic:" + entry.getKey());
                for (String msg : entry.getValue().values()) {
                    logger.info("Suchit Consumed Message: " + msg);
                }
            }
        }


}
