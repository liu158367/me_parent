package com.me;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ClassPathXmlApplicationContext apac = new ClassPathXmlApplicationContext(new String[]{"spring/spring-mvc.xml"});
        apac.start();


    }
}
