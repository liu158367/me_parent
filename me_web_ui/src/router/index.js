import Vue from 'vue'
import Router from 'vue-router'
import Findex from '@/components/f/layout/Layout'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'findex',
      component: Findex
    }
  ]
})
