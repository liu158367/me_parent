package com.me.blog.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class BlogEntity implements Serializable {
    private int id;
    private String title;
    private String direction;
    private String contenturl;
    private String renderurl;
    private long creattime;
    private int saw;
    private int star;
    private String type;

}
